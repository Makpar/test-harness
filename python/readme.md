## Running
1. With Pipenv
   - pipenv run behave
2. With specific features
   - pipenv run behave -t "@API"
3. With html out: 
   - pipenv run behave -f html -o behave-report.html
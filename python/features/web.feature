@Web
Feature: Web
#https://**.makpar-innovation.com
Scenario: Open Browser and site
    When navigate browser to "https://ccc-dev.makpar-innovation.com/"
    And verify that artists site displays search

Scenario: Search by artist
    When set the artist search as "Robert Moir"
    And click the artist search submit button
    Then the artist results should be displayed

Scenario: Verify artist info name
    Given that artists results is displayed
    Then verify that the artists results contains artist "Robert Moir"

Scenario: Select artist
    Given select artists result named "Robert Moir"
    Then verify that displayed artists result is "Robert Moir"

Scenario: Verify artist info name
    Given that artist results is displayed
    Then verify that displayed artists result name is "Robert Moir"
    And verify that displayed artists result nationality is "No Information Available"

Scenario: Verify artist info Dates
    Given that artist results is displayed
    Then verify that displayed artists result life date is "1917 - 1981"

Scenario: Verify artworks
    Given that artist results is displayed
    Then verify that displayed artists result has at least 1 artwork displayed
    And  verify that displayed artists result artworks contains "The Clock"
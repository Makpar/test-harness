@API
Feature: Api

Scenario: Setup
    Given using API url "https://api-ccc-dev.makpar-innovation.com"

Scenario: Post Search
    When post API "search" with body "{'artist_name':'Robert Moir'}"
    Then verify API response is 200

Scenario: Post Search not found
    When post API "search" with body "{'artist_name':'Nota Person'}"
    Then verify API response is 404

Scenario: Post Search 400 no artist
    When post API "search" with body "{}"
    Then verify API response is 400

Scenario: Post Search 400 no artist name
    When post API "search" with body "{'artist_name': ''}"
    Then verify API response is 404
from selenium import webdriver
import logging
from sys import platform as _platform
import os

from selenium.webdriver.chrome.webdriver import WebDriver
from util import set_local,get_local,fpath
from types import MethodType

DOWNLOAD_DIR = 'download_dir'
DRIVER_PATH = 'driver_path'
DEFAULT_DRIVER_PATH = fpath(os.path.abspath(os.path.join(os.path.dirname(__file__),os.pardir)),'drivers')

def set_drivers_path(path):
    set_local(DRIVER_PATH,path)

def set_download_dir(path):
    set_local(DOWNLOAD_DIR,path)

def driver(browser = 'chrome', headless=None, capabilities: dict = None) -> webdriver.Remote:
    """Get a web driver that is thread local and has additional functionality. 
        If a driver already exists it will return the current instance. 
    Args:
        browser (str, optional): The browser type to use. If none is provided it will check CFG for "defaultBrowser" or use chrome. Defaults to None.
        headless (bool, optional): Is this a headless browser. If none is provided it will check CFG for "headless" or false. Defaults to None.
        capabilities (dict, optional): [description]. Defaults to None.

    Returns:
        webdriver.Remote: The new or existing webdriver.
    """
    webdriver = get_local('driver')
    if webdriver is None:
        set_local('driver', __create_driver(browser, headless, capabilities))
        return get_local('driver')
    return webdriver


def quit(self):
    """
    Quit the current webdriver so that a new one can be created.
    """
    self.quit_browser()
    set_local('driver',None)

def quit_if_driver():
    """
    Quit the current webdriver if it exists
    """
    webdriver = get_local('driver')
    if webdriver is not None:
        webdriver.quit()

def __chrome(headless=False, capabilities: dict = None):
    logging.getLogger(__name__).log(logging.INFO,"Starting chrome driver - headless: {},extra capabilities: {}".format(headless, capabilities))
    options = webdriver.ChromeOptions()
    options.headless = headless
    preferences = {"download.default_directory":  get_local(DOWNLOAD_DIR, "./downloads"),
               "directory_upgrade": True,
               "download.prompt_for_download": False}
    options.add_experimental_option("prefs", preferences)
    __addDriverOptions(options, capabilities)
    dpath = get_local(DRIVER_PATH, DEFAULT_DRIVER_PATH)
    if os.path.isdir(dpath):
        return webdriver.Chrome(executable_path=fpath(__getOSPath(dpath), "chromedriver"), options=options)
    else:
        return webdriver.Chrome(options=options)
    
def __create_driver(browser,headless=False, capabilities: dict = None):
    webdriver = _browserDriver.get(browser)(
        headless=headless, capabilities=capabilities)
    webdriver.quit_browser = webdriver.quit
    webdriver.quit = MethodType(quit, webdriver)
    webdriver.currentURL = None
    return webdriver

def __addDriverOptions(options, capabilities: dict):
    if capabilities:
        for d in capabilities:
            options.set_capability(d, capabilities[d])

_browserDriver = {
    "chrome": __chrome
}

def __getOSPath(path):
    if _platform == "linux" or _platform == "linux2":
        return fpath(path,"linux")
    elif _platform == "darwin":
        return fpath(path,"mac")
    elif _platform == "win32" or _platform == "cygwin":
        return fpath(path,"win")

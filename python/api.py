
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Imports
import json
import os
import xml.etree.ElementTree
import requests
import logging

# ---------------------------------------------------------------------------
# API access
# ------------------------------------------------------------------------------

class API():

    def __init__(self, url:str, username:str=None, password:str=None, authorization:str=None):
        """
        API access

        :param url: the URL
        :param username: username
        :param password: password
        :param authorization: a authorization token to use.
        """
        if url.startswith('localhost') or url.startswith('127.0.0.1'):
            url = 'http://{}'.format(url)
        elif url.startswith('http://'):
            url = url
        elif not url.startswith('https://'):
            url = 'https://{}'.format(url)
        if url.endswith('/'):
            url = url[:-1]
        
        self.api_url = url
        self.username = username
        self.password = password
        self.authorization = authorization
        self.isAuthKey = True if authorization else False
        self.response = None
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.log(logging.INFO,u'ISAUTH: {}'.format(self.isAuthKey))

    def get(self, endpoint, params=None, headers=None):
        """GET from API

        :param endpoint: API endpoint
        :param params: HTTP parameters will convert from string (dict,str), defaults to None.
        :param headers: HTTP headers (dict), defaults to None.
        :return: A response body.
        """
        url = '{}{}'.format(self.api_url, endpoint) if endpoint.startswith('/') else '{}/{}'.format(self.api_url, endpoint)
        self.log.log(logging.INFO, u'API GET: {} - Params: {}'.format(url, params))
        try:
            self.response = requests.get(
                url, params=self.__to_dict(params), **self.__get_kwargs(headers))
        except requests.RequestException as e:
            self.log.log(logging.ERROR, u'Request response: {}'.format(e.response.text))
            raise e
        else:
            if self.response.status_code == 200:
                self.log.log(logging.INFO, u'SUCCESS status: {}, response: {}'.format(str(self.response.status_code),self.response.text))
            else:
                self.log.log(logging.INFO, u'FAILURE status: {}, response: {}'.format(str(self.response.status_code),self.response.text))
            return self.get_response_body()

    def post(self, endpoint, json=None, data=None, params=None, headers=None, files=None):
        """Post to API

        :param endpoint: API endpoint
        :param json: Json payload will load from file and convert from string (dict,str), defaults to None.
        :param data: HTTP payload will load from file (dict,str,bytes), defaults to None.
        :param params: HTTP parameters will convert from string (dict,str), defaults to None.
        :param headers: HTTP headers (dict), defaults to None.
         :param files: (optional) Dictionary of ``'name': file-like-objects``, defaults to None.
        :return: The body of the response as a JSON, XML ELTREE or string based on the content-type header.
        """
        url = '{}{}'.format(self.api_url, endpoint) if endpoint.startswith('/') else '{}/{}'.format(self.api_url, endpoint)
        self.log.log(logging.INFO,
             u'API POST: {} - Params: {} - Data: {}'.format(url, params, data))
        try:
            self.response = requests.post(url=url, data=self.__get_data(data), json=self.__get_json(json), params=self.__to_dict(params),
                                         files=files,**self.__get_kwargs(headers))
        except requests.RequestException as e:
            self.log.log(logging.ERROR, u'Request response: {}'.format(e.response.text))
            raise e
        else:
            if self.response.status_code in range(200, 227):
                self.log.log(logging.INFO, u'SUCCESS status: {}, response: {}'.format(str(self.response.status_code),self.response.text))
            else:
                self.log.log(logging.INFO, u'FAILURE status: {}, response: {}'.format(str(self.response.status_code),self.response.text))
            return self.get_response_body()

    def put(self, endpoint, json=None, data=None, params=None, headers=None, files=None):
        """PUT to API

        :param endpoint: API endpoint
        :param json: Json payload will load from file and convert from string (dict,str), defaults to None.
        :param data: HTTP payload will load from file (dict,str,bytes), defaults to None.
        :param params: HTTP parameters will convert from string (dict,str), defaults to None.
        :param headers: HTTP headers (dict), defaults to None.
         :param files: (optional) Dictionary of ``'name': file-like-objects``, defaults to None.
        :return: The body of the response as a JSON, XML ELTREE or string based on the content-type header.
        """
        url = '{}{}'.format(self.api_url, endpoint) if endpoint.startswith('/') else '{}/{}'.format(self.api_url, endpoint)
        self.log.log(logging.INFO,
             u'API PUT: {} - Params: {} - Data: {}'.format(url, params, data))
        try:
            self.response = requests.put(url=url, data=self.__get_data(data), json=self.__get_json(json), params=self.__to_dict(params),
                                         files=files,**self.__get_kwargs(headers))
        except requests.RequestException as e:
            self.log.log(logging.ERROR, u'Request response: {}'.format(e.response.text))
            raise e
        else:
            if self.response.status_code in range(200, 227):
                self.log.log(logging.INFO, u'SUCCESS status: {}, response: {}'.format(str(self.response.status_code),self.response.text))
            else:
                self.log.log(logging.INFO, u'FAILURE status: {}, response: {}'.format(str(self.response.status_code),self.response.text))
            return self.get_response_body()

    def __to_dict(self, value):
        """Converts a string into a dict.
        Args:
            value (str, obj): The value to convert

        Returns:
            dict: Returns the new dict or the passed in object if it was not a string.
        """
        if isinstance(value, str):
            json_str = value.replace("'", '"').replace("True", '"True"').replace("False", '"False"').replace("null", '"null"')
            try:
                return json.loads(json_str)
            except Exception:
                pass
        return value

    def __get_kwargs(self, headers=None):
        if headers is not None and self.isAuthKey:
            headers['Authorization'] = self.authorization
        elif self.isAuthKey:
            headers= {"Authorization":self.authorization}
        return {"headers": headers} if self.isAuthKey or not self.username else {"headers": headers, "auth": (self.username, self.password)}

    def __get_data(self, value):
        if isinstance(value,str):
            if os.path.isfile(value):
                return os.open(value)
        return value

    def get_response_body(self):
        """Get the body of the API response.
           Coverts the reponse depending on the content-type header.

        Returns:
            ElementTree, list, str, JSON: The response body converted to the content-type header.
        """
        if self.response:
            try:
                content_type = self.response.headers['Content-Type']
                if 'json' in content_type:
                    return self.response.json()
                elif 'xml' in content_type:
                    return xml.etree.ElementTree.fromstring(self.response.text)
                else:
                    return self.response.text
            except Exception:
                return self.response.text
        return None
    
    def __get_json(self,value):
        """Convert a string to JSON object or load a JSON file.

        :param value: file path or a string json.
        :return: dict
        """
        if isinstance(value, str):
            jsn = None
            self.log.log(logging.INFO, u'Json type: {}, value {}'.format(type(value),value))
            try:
                if os.path.isfile(str):
                    try:
                        with open(str) as f:
                            jsn = json.load(f)
                    except IOError as e:
                        raise Exception("File", e)
            except:
                pass
            if jsn == None:
                try:
                    #convert if dict
                    value = value.replace("'", '"').replace("True", '"True"').replace("False", '"False"').replace("null", '"null"')
                    jsn = json.loads(value)
                except Exception:
                    return None
            return jsn
        return value

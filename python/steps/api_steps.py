from util import set_local, get_local, CFG
from api import API
from behave import given, when, then
import uuid

LOCAL_API_USER = 'api_user'
LOCAL_API_AUTH = 'api_authorization'
LOCAL_API_RESPONSE = 'api_response'
LOCAL_API_BODY = 'api_body'


def set_response(response):
    set_local(LOCAL_API_RESPONSE, response)


def get_response():
    return get_local(LOCAL_API_RESPONSE)


def get_hostname():
    return get_local('api_hostname')


def set_hostname(hostname):
    return set_local('api_hostname', hostname)


def get_user():
    return get_local(LOCAL_API_USER)

def set_user(user):
    u = CFG().get_user(user)
    set_local(LOCAL_API_USER, u)
    set_local('api_user_changed', True)

def get_auth():
    return get_local(LOCAL_API_AUTH)

def set_auth(auth):
    set_local(LOCAL_API_AUTH,auth)
    set_local('api_user_changed', True)

def is_user_updated():
    return get_local('api_user_changed', False)

def api(hostname=None) -> API:
    last = get_hostname()
    newconn = is_user_updated()
    if last == None:
        hostname = hostname if hostname != None else "default"
    if hostname != None and last != hostname:
        newconn = True
        set_hostname(hostname)
    else:
        hostname = last

    if newconn:
        auth = get_user()
        auth['authorization'] = get_auth()
        #**get_user(), authorization=get_auth()
        set_local('api', API(hostname, **auth))
        set_local('api_user_changed', False)
    return get_local('api')

@given(u'using API user "{user}" for url "{host}"')
def using_api(context,user,host):
    set_user(user)
    api(host)

@given(u'using API url "{host}"')
def using_api(context,host):
    set_user(None)
    api(host)

@given(u'get AWS API authorization for endpoint "{endpoint}" using cfg json body "{body}"')
@when(u'get AWS API authorization for endpoint "{endpoint}" using cfg json body "{body}"')
def get_aws_auth_endpoint(context,endpoint,body):
    jsn = CFG().get_json(body)
    api().post(endpoint,json=jsn)
    
@when(u'get AWS API authorization for url "{url}" using cfg json body "{body}"')
def get_aws_auth(context,url,body):
    set_user(None)
    jsn = CFG().get_json(body)
    api(url).post("",json=jsn)

@then(u'get API "{endpoint}" with noted docket id {num:d}')
@when(u'get API "{endpoint}" with noted docket id {num:d}')
def get_api_w_docket(context,endpoint,num):
    li = get_local("docket_ids")
    item = li[int(num)]
    api().get(endpoint+"/"+item["docket_id"])

@when(u'get API "{endpoint}"')
def get_api(context,endpoint):
    api().get(endpoint)

@when(u'post API "{endpoint}" with body "{body}" replace "{replace}" with uuid')
def post_api_replace_uuid(context,endpoint,body,replace):
    body = body.replace(replace,str(uuid.uuid4()))
    api().post(endpoint,body)

@when(u'post API "{endpoint}" with body "{body}"')
def post_api(context,endpoint,body):
    api().post(endpoint,body)

@when(u'put API "{endpoint}" with body "{body}" replace "{replace}" with uuid')
def put_api_replace_uuid(context,endpoint,body,replace):
    body = body.replace(replace,str(uuid.uuid4()))
    api().put(endpoint,body)

@when(u'put API "{endpoint}" with body "{body}"')
def put_api(context,endpoint,body):
    api().put(endpoint,body)

@then(u'verify API response is {status:d}')
def verify_status(context,status):
    assert str(api().response.status_code) == str(status)

@given(u'note AWS authorization')
@when(u'note AWS authorization')
@then(u'note AWS authorization')
def note_aws_authorization(context):
    body = api().get_response_body()
    set_auth("Bearer "+str(body['AuthenticationResult']['IdToken']))

@when(u'note API agency docket ids')
@then(u'note API agency docket ids')
def note_agency_docket(context):
    body = api().get_response_body()
    set_local("docket_ids",body["results"])
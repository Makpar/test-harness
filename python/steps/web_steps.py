from threading import Thread
from behave import given, when, then
from driver import driver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.login import Login
from pages.main import Main
import time

from util import CFG

__login = Login()
__main = Main()

@when(u'navigate browser to "{url}"')
def navigate(context,url):
    driver(headless=CFG().is_headless()).get(url)

@given(u'wait for {secs:d} seconds')
@when(u'wait for {secs:d} seconds')
@then(u'wait for {secs:d} seconds')
def wait_for_seconds(context,secs):
    time.sleep(secs)

@then(u'close browser')
@when(u'close browser')
def close_browser(context):
    driver().quit()

@when(u'login to site as "{user}"')
def login_as_user(context,user):
    u = CFG().get_user(user)
    time.sleep(5)
    __login.login(u['username'],u['password'])


@when(u'click logout button')
def click_logout(context):
    assert __main.wait_for_logout()
    assert __main.logout()

@when(u'verify that site displays unauthenticated view')
@then(u'verify that site displays unauthenticated view')
def verify_unauth(context):
    assert __login.verify_unauth()

@when(u'verify that artists site displays search')
def verify_artists_displayed_search(context):
    assert __main.wait_for_search()


@when(u'set the artist search as "{artist}"')
def set_the_artist_serch(context,artist):
    assert __main.set_search(artist)


@when(u'click the artist search submit button')
def click_search_submit(context):
    assert __main.click_search_submit()
    time.sleep(2)

@then(u'the artist results should be displayed')
@given(u'that artist results is displayed')
@given(u'that artists results is displayed')
def artists_results_displayed(context):
    assert __main.wait_for_results()

@then(u'verify that the artists results contains artist "{artist}"')
def verify_artists_results_contains(context,artist):
    assert __main.get_results_artist(artist)

@given(u'select artists result named "{name}"')
def select_artist_result_named(context,name):
    assert __main.select_results_artist(name)

@then(u'verify that displayed artists result is "{name}"')
def verify_that_displayed_result_is(context,name):
    assert name.lower() in __main.get_selected_artist_name().lower()

@then(u'verify that displayed artists result name is "{value}"')
def artists_result_first_name(context,value):
    assert value.lower() in __main.get_selected_artist_name().lower()

@then(u'verify that displayed artists result life date is "{value}"')
def artists_result_life(context,value):
    assert value.lower() in __main.get_selected_artist_life_date().lower()

@then(u'verify that displayed artists result nationality is "{value}"')
def artists_result_nation(context,value):
    assert value.lower() in __main.get_selected_artist_nationality().lower()

@then(u'verify that displayed artists result has at least {size:d} artwork displayed')
def artists_result_artworks_size(context,size):
    assert len(__main.get_selected_artist_art_rows()) > int(size)

@then(u'verify that displayed artists result artworks contains "{name}"')
def artists_result_artworks_row_name(context,name):
    assert __main.get_selected_artist_art_row_title(name)
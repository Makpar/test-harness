
from util import CFG, set_local,get_local
from .locators.main import Main as loc
from driver import driver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC, wait

class Main:

    __SELECTED_ARTIST = "selected_artist"

    def logout(self):
        try:
            element = WebDriverWait(driver(), 1).until(
                EC.presence_of_element_located((By.XPATH, loc.LOGOUT_XP))
            )
            element.click()
            return True
        except:
            return False

    def wait_for_logout(self):
        try:
            element = WebDriverWait(driver(), 10).until(
                EC.presence_of_element_located((By.XPATH, loc.LOGOUT_XP))
            )
            return True
        except:
            return False

    
    def set_search(self,name):
        try:
            self.wait_for_search()
            ele = driver().find_element_by_xpath(loc.SEARCH_XP)
            ele.send_keys(name)
            return True
        except:
            return False

    def wait_for_search(self):
        try:
            element = WebDriverWait(driver(), 5).until(
                EC.presence_of_element_located((By.XPATH, loc.SEARCH_XP))
            )
            return True
        except:
            return False

    def click_search_submit(self):
        try:
            self.wait_for_submit()
            ele = driver().find_element_by_xpath(loc.SEARCH_BUTTON_XP)
            ele.click()
            return True
        except:
            return False

    def wait_for_submit(self):
        try:
            element = WebDriverWait(driver(), 1).until(
                EC.presence_of_element_located((By.XPATH, loc.SEARCH_BUTTON_XP))
            )
            return True
        except:
            return False

    
    def get_results_artist(self,artist):
        try:
            eles = driver().find_elements_by_xpath(loc.ARTIST_ITEM_XP)
            for ele in eles:
                name = ele.find_element_by_xpath(loc.ARTIST_ITEM_NAME_XP)
                if name:
                    if artist.lower() in name.text.lower():
                        return ele
            return None
        except:
            return None

    def wait_for_results(self):
        try:
            element = WebDriverWait(driver(), 30).until(
                EC.presence_of_element_located((By.XPATH, loc.ARTIST_ITEM_XP))
            )
            return True
        except Exception as e:
            print (e)
            return False

    def select_results_artist(self,artist):
        try:
            ele = self.get_results_artist(artist)
            if ele:
                set_local(self.__SELECTED_ARTIST,ele)
                return True
            return False
        except:
            return False

    def get_selected_artist_name(self):
        try:
            ele = get_local(self.__SELECTED_ARTIST)
            if ele:
                return ele.find_element_by_xpath(loc.ARTIST_ITEM_NAME_XP).text
            return None
        except:
            return None

    def get_selected_artist_life_date(self):
        try:
            ele = get_local(self.__SELECTED_ARTIST)
            if ele:
                return ele.find_element_by_xpath(loc.ARTIST_ITEM_LIFE_DATE_XP).text
            return None
        except:
            return None
        
    def get_selected_artist_nationality(self):
        try:
            ele = get_local(self.__SELECTED_ARTIST)
            if ele:
                return ele.find_element_by_xpath(loc.ARTIST_ITEM_NATION_XP).text
            return None
        except:
            return None

    def get_selected_artist_art_rows(self):
        try:
            ele = get_local(self.__SELECTED_ARTIST)
            if ele:
                return ele.find_elements_by_xpath(loc.ARTIST_ITEM_ART_TABLE_ROW)
            return []
        except:
            return []

    def get_selected_artist_art_row_title(self,title):
        try:
            rows = self.get_selected_artist_art_rows()
            for row in rows:
                c1 = row.find_element_by_xpath("./td[1]")
                if c1:
                    if title.lower() in c1.text.lower():
                        return row
            return None
        except Exception as e:
            print(e)
            return None
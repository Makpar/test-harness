

class Login:
    #Amplify only
    AMP_AUTH_CONTAINER = "//*[name()='amplify-auth-container']"#"//*[name()='amplify-auth-container']"
    AMP_SHADOW_PARENT_1 = "amplify-authenticator"#"//*[name()='amplify-authenticator']"
    AMP_SHADOW_PARENT_2 = "amplify-sign-in"
    AMP_USERNAME_ID = "username"
    AMP_PASSWORD_ID = "password"
    AMP_SIGN_IN = "slot > amplify-button"

    USERNAME_ID = "formBasicEmail" #//input[@id='formBasicEmail']
    PASSWORD_ID = "formBasicPassword"
    SIGN_IN = "//button[@type='submit' and text()='Submit']"
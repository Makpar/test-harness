

class Main:
    LOGOUT_XP= "//a/button[text()='Logout']"

    SEARCH_XP = "//input[@placeholder='Search']"
    SEARCH_BUTTON_XP = "//Button[text()='Search']"

    ARTIST_ITEM_XP = "//div[contains(@class,'mt-4')]/div"
    ARTIST_ITEM_NAME_XP = "./div[1]/h4"
    ARTIST_ITEM_NATION_XP = "./div[1]/p[1]"
    ARTIST_ITEM_LIFE_DATE_XP = "./div[1]/p[2]"
    ARTIST_ITEM_BIO_XP = "./div[1]/p[3]"

    ARTIST_ITEM_ART_TABLE = "./div[2]/div/table"
    ARTIST_ITEM_ART_TABLE_ROW = "./div[2]/div/table/tbody/tr"
    ARTIST_ITEM_ART_TABLE_ROW_COL = "./div[2]/div/table/tbody/tr/td"
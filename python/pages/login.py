
from .locators.login import Login as loc
from driver import driver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Login:
    
    def expand_shadow_element(self,element):
        shadow_root = driver().execute_script('return arguments[0].shadowRoot', element)
        return shadow_root

    def login_amplify(self,username, password):
        #So this is needed because the form is hidden in shadow dom.
        sr = self.expand_shadow_element(driver().find_element_by_css_selector(loc.AMP_SHADOW_PARENT_1))
        sr = self.expand_shadow_element(sr.find_element_by_css_selector(loc.AMP_SHADOW_PARENT_2))
        un = sr.find_element_by_id(loc.AMP_USERNAME_ID)
        pw = sr.find_element_by_id(loc.AMP_PASSWORD_ID)
        un.send_keys(username)
        pw.send_keys(password)
        WebDriverWait(driver(), 2).until(lambda ignore:
            sr.find_element_by_css_selector(loc.AMP_SIGN_IN).is_enabled()
            , "Element is not clickable")
        sr.find_element_by_css_selector(loc.AMP_SIGN_IN).click()
    
    def login(self,username, password):
        un = driver().find_element_by_id(loc.USERNAME_ID)
        pw = driver().find_element_by_id(loc.PASSWORD_ID)
        un.send_keys(username)
        pw.send_keys(password)
        WebDriverWait(driver(), 2).until(lambda ignore:
            driver().find_element_by_xpath(loc.SIGN_IN).is_enabled()
            , "Element is not clickable")
        driver().find_element_by_xpath(loc.SIGN_IN).click()
    
    def verify_unauth_amp(context):
        try:
            element = WebDriverWait(driver(), 40).until(
                EC.presence_of_element_located((By.XPATH, loc.AMP_AUTH_CONTAINER))
            )
            return True
        except:
            return False

    def verify_unauth(context):
        try:
            element = WebDriverWait(driver(), 40).until(
                EC.presence_of_element_located((By.ID, loc.AMP_USERNAME_ID))
            )
            return True
        except:
            return False
import threading
import os
import json

thread_local = threading.local()
lock = threading.Lock()


def get_local(name: str, default_to = None):
    return getattr(thread_local, name, default_to)

def set_local(name: str, value:object):
    setattr(thread_local, name, value)

class CFG():

    def __init__(self, init="init.json"):
        self.KEY = "cfg"
        if get_local(self.KEY,None) is None:
            initobj = self.__load_json_cfg(init)
            print(initobj)
            if "cfg" in initobj:
                set_local(self.KEY,self.__load_json_cfg(initobj.get("cfg")))
            else:
                raise Exception("No cfg key in init file.")
            
        
    def get(self, key:str,default=None):
        cfg = get_local(self.KEY)
        if cfg:
            return cfg.get(key,default)
        return default

    def set(self, key:str,value):
        cfg = get_local(self.KEY)
        if cfg:
            cfg[key] = value

    def get_user(self, key:str=None):
        users = self.get("users",{})
        user = {"username":key,"password":None}
        if key in users:
            user = users.get(key,user)
        if not "password" in user or user.get("password") is None:
            user["password"] = self.get_password("default")
        else:
            pswrd = user.get("password")
            user["password"] = self.get_password(pswrd,pswrd)
        return user

    def get_password(self, key:str,default=""):
        passwords = self.get("passwords",{})
        return passwords.get(key,default)
    
    def get_json(self, key:str,default={}):
        jsons = self.get("jsons",{})
        return jsons.get(key,default)

    def __load_json_cfg(self, path:str):
        if os.path.isfile(path):
            try:
                with open(path) as f:
                    return json.load(f)
            except IOError as e:
                raise Exception("File", e)
    
    def is_headless(self, default=False):
        return self.get("browser_headless",default)

def fpath(path:str, filename:str):
    """Updates a path with a filename. Handles os seperators.

    Args:
        path (str): The base path to use.
        filename (str): The filename to add.

    Returns:
        str: The resulting file path.
    """
    if os.path.isdir(path):
        if path.endswith(os.sep):
            return path + filename
        else:
            return path + os.sep + filename
    return path
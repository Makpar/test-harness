The *python/features/api.feature.template* is hooked into [Sandbox/gateway-lambdas](https://bitbucket.org/Makpar/gateway-lambdas/src/Dev/) and runs API tests against each deployment. The *python/features/web.feature.template* is hooked into [Sandbox/cloudfront-s3](https://bitbucket.org/Makpar/cloudfront-s3/src/Dev/) and runs Web tests against deployment.
# Setup

1. Install dependencies

```
pip install pipenv && pipenv install
```

2. Copy *python/.sample.env* into *python/.env* and update your credentials in the **API_USERNAME** and **API_PASSWORD** variables with the email and password you have been provisioned through **Cognito**. If you need an account, email/contact gmoore@makpar.com or pcofranscesco@makpar.com. Update the **API_HOST_URL** to point to the environment you are testing. 

The */python/.env* file has been added to the *.gitignore*, but keep in mind this file should never been committed, since it contains your credentials. 

3. Run the *python/configure* script

```
./python/configure
```

This will inject your credentials into a new file, */python/credentials.json*. For the same reason as never committing the *.env* file, never commit this file. It too has already been add to the *.gitignore*.

In addition, this script two other important things: it will configure the */python/init.json* file to point to this newly generated credentials file, And it will inject **API_HOST_URL** in */python/features/api.feature.template* and copy the result into */python/features/api.features*. This is done to parameterize the host url in the pipeline for different environments. 

4. Run tests,

```
pipenv run behave
```